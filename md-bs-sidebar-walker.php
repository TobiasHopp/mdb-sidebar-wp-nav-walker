<?php
/* Material Design for Bootstrap
   Left Sidebar Walker Class

   This class works so far, but does NOT open
   the collapsed menu if the current page is
   one of those items.

   If someone could help to fix this, that would be great.
   
   Thanks
   Toby

*/

/*
	wp_nav_menu()
	(function names that manages the menu output)

	<div class="menu-container">
		<ul>					// start_lvl()
			<li><a href="#">    // start_el()
				</a></li>		// end_el()
		</ul>					// end_lvl()
	</div>

	Include this file in your functions.php and then call it via
	<?php
		wp_nav_menu([
			'menu'            => 'left_sidebar', // Your menu location
			'theme_location'  => 'left_sidebar', // Your menu location
			'container'       => false,
			'menu_id'         => 'c4t-sidebar-menu',
			'menu_class'      => 'collapsible collapsible-accordion',
			'depth'           => 2,
			'walker'          => new MD_BS_Sidebar_Walker() // name of our walker
		]);
	?>
*/

if (!class_exists('MD_BS_Sidebar_Walker')){
	class MD_BS_Sidebar_Walker extends Walker_Nav_Menu {

		function start_lvl(&$output, $depth = 0, $args = array() ) { // ul

			$indent = str_repeat("\t", $depth);
			$submenu = ($depth > 0) ? ' sub-menu' : '';
			$output .= "\n$indent<div class=\"collapsible-body\">\n";
			$output .= "\n$indent<ul>\n";

		}

		function start_el(&$output, $item, $depth = 0, $args = array(), $id = 0) { // li a span

			$indent = ($depth) ? str_repeat("\t", $depth) : '';

			$li_attributes = '';
			$class_names = $value = '';

			$classes = empty($item->classes) ? array() : (array) $item->classes;

			$classes[] = ($args->walker->has_children) ? 'dropdown' : '';
			$classes[] = ($item->walker->current || $item->walker->current_item_anchestor) ? 'active' : '';
			$classes[] = 'menu-item-' . $item->ID;

			if($depth && $args->walker->has_children) {
				$classes[] = 'dropdown-submenu';
			}

			$class_names = join(' ', apply_filters('nav_menu_css_class', array_filter($classes),  $item, $args));
			$class_names = ' class="'.esc_attr($class_names).'"';

			$id = apply_filters('nav_menu_item_id', 'menu-item-'.$item->ID, $item, $args);
			$id = strlen($id) ? ' id="'.esc_attr($id).'"' : '';

			$output .= $indent .'<li' . $id . $value . $class_names . $li_attributes . '>';

			if($item->url == '#'){
				$attributes = '';
			} else {
				$attributes = ' href="'.esc_attr($item->url).'"';
			}
			
			$attributes .= !empty($item->target) ? ' target="'.esc_attr($item->target).'"' : '';
			$attributes .= !empty($item->xfn) ? ' rel="'.esc_attr($item->xfn).'"' : '';
			$attributes .= !empty($item->attr_title) ? ' title="'.esc_attr($item->attr_title).'"' : '';

			$attributes .= ($args->walker->has_children) ? ' class="collapsible-header waves-effect arrow-r"' : 'collapsible-header waves-effect arrow-r active';

			$item_output = $args->before; // defaults by WP
			$item_output .= '<a' . $attributes . '><i class="fa fa-angle-right"></i>';
			$item_output .= $args->link_before . apply_filters('the_title', $item->title, $item->ID) .$args->link_after;
			$item_output .= ($depth == 0 && $args->walker->has_children) ? '<i class="fa fa-angle-down rotate-icon"></i></a>' : '</a>';

			$item_output .= $args->after; // defaults by WP
			
			$output .= apply_filters('walker_nav_menu_start_el', $item_output, $item, $depth, $args);

		}

		/*
		function end_el() {  // closing li a span

		}

		function end_lvl() { // closing ul

		} */

	}
}